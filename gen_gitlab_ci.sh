#!/bin/bash

function print_variant() {
    variant="$1"
    allow_failure="$2"
    cat <<EOF

build_$variant:
  stage: build
  tags:
    - docker
    - privileged
  script:
    - apt-get update
    - apt-get install -y --no-upgrade eatmydata
    - eatmydata apt-get install -y --no-upgrade apt-utils qemu-user-static qemu-system qemu-utils mmdebstrap debian-archive-keyring debian-ports-archive-keyring libguestfs-tools linux-image-amd64 tar
    - mkdir artifacts
    - http_proxy="http://6.22.22.129:3142/" DIR=artifacts DISK_SIZE=0 ./create.sh $variant
    - ls -hl artifacts
  allow_failure: $allow_failure
  artifacts:
    paths:
      - artifacts
    expire_in: 1 year

convert_$variant:
  stage: convert
  tags:
    - shell
  dependencies:
    - build_$variant
  needs: ["build_$variant"]
  script:
    - virt-make-fs --format=qcow2 --size=10G --partition=gpt --type=ext4 --label=rootfs artifacts/image.tar.gz artifacts/image.qcow2
    - qemu-img convert -f qcow2 artifacts/image.qcow2 -O qcow2 artifacts/image2.qcow2
    - mv artifacts/image2.qcow2 artifacts/image.qcow2
    - rm artifacts/image.tar.gz
    - ls -hl artifacts
  allow_failure: $allow_failure
  artifacts:
    paths:
      - artifacts
    expire_in: 1 year
EOF
}

cat <<EOF
image: registry.gitlab.com/giomasce/dqib/testenv

variables:
  GIT_STRATEGY: fetch
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_DEPTH: "3"

before_script:
  - cat /proc/cpuinfo
  - cat /proc/meminfo

stages:
  - build
  - convert
EOF

for variant in i386-pc amd64-pc mipsel-malta mips64el-malta armel-raspi2 armhf-virt arm64-virt ppc64el-pseries s390x-virt m68k-q800 powerpc-g3beige ppc64-pseries riscv64-virt ; do
    print_variant $variant "false"
done

for variant in alpha-clipper hppa-hppa sh4-r2d sparc64-sun4u ; do
    print_variant $variant "true"
done
