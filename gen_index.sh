#!/bin/bash

for variant in i386-pc amd64-pc mipsel-malta mips64el-malta armel-virt armhf-virt arm64-virt ppc64el-pseries s390x-virt alpha-clipper hppa-hppa m68k-q800 powerpc-g3beige ppc64-pseries riscv64-virt sh4-r2d ; do
    cat <<EOF
      <li><a href="https://gitlab.com/api/v4/projects/giomasce%2Fdqib/jobs/artifacts/master/download?job=convert_$variant">Images for $variant</a></li>
EOF
done
